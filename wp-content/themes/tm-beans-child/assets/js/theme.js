!(function( $ ) {

    $(document).ready(function(){

        $('#grid-filter li a').click(function(){
            //On récupère le slug du filtre cliqué
            $filtre = $(this).parent().attr('data-uk-filter');
            //On récupère les données depuis la page des catégories
            $.ajax({
                url: '/category/' + $filtre,
                data: {cat: 'cat'},
                method: 'POST'
            }).done(function(data) {
                //On supprime le conteneur des posts courant
                $('.tm-content').remove();
                var success =  $($.parseHTML(data)).find(".tm-content");
                //On insère le noveau conteneur des posts
                success.insertAfter($('#grid-filter'));
                setTimeout(update_grid, 200);
            });
            function update_grid(){
                UIkit.grid($('[data-uk-grid]'), {gutter: 20});
            }
            return false;
        });

    });

    $( document ).on( 'click', '.tm-primary-menu .tm-search-toggle', function() {

        var searchSelector = '.tm-primary-menu .tm-search';

        $( searchSelector ).toggle();
        $( '.tm-primary-menu .uk-navbar-nav' ).toggle();

        if ( $( searchSelector ).is( ':visible' ) ) {

            $( searchSelector ).find( 'input' ).focus();
            $( this ).find( 'i').removeClass( 'uk-icon-search' ).addClass( 'uk-icon-close' );

        } else {

            $( this ).find( 'i').removeClass( 'uk-icon-close' ).addClass( 'uk-icon-search' );
        }

    } );

} )( window.jQuery );