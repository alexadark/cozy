<?php


beans_add_smart_action( 'init', 'wst_post_type_support' );

function wst_post_type_support() {

	remove_post_type_support( 'page', 'comments' );

}

add_filter( 'jpeg_quality', create_function( '', 'return 60;' ) );


beans_remove_action( 'beans_comments_closed' );
//Post title
beans_modify_action_priority( 'beans_post_title', 3 );
beans_add_smart_action( 'wp', 'wst_setup_document' );
function wst_setup_document() {
	if ( ! is_single() ) {

		beans_add_smart_action( 'beans_post_header_prepend_markup', 'wst_display_seo_title' );
		function wst_display_seo_title() {
			$seo_title = get_seo_title(); ?>
			<h2 class="uk-article-title grid-title"
			    itemprop="headline">
				<a href="<?php the_permalink(); ?>"
				   title="<?php the_title(); ?>"
				   rel="bookmark"><?php echo $seo_title; ?></a>
			</h2>
			<?php

		}

		beans_remove_output( 'beans_post_title_text' );
	}
	if ( is_home() || is_category() ) {
		beans_remove_action( 'beans_post_meta' );
		add_filter( 'beans_layout', 'wst_fw_force_layout' );
		function wst_fw_force_layout() {
			return 'c';
		}
	}

}


add_filter( 'the_content', 'wst_modify_content' );
function wst_modify_content( $content ) {
	// use excerpt if available, else fallback to the post content

//	Add uikit arti-lead class if on the single post layout
	if ( is_singular() ) {
		return $content;
	} //Replace content with seo desc and more link on the blog index
	elseif ( is_home() || is_archive() ) {
		$desc = get_seo_description();
		?>
		<?php echo $desc; ?>

		<?php
//		if(is_archive()){
//			echo beans_post_more_link();
//		}


	}

}

beans_add_attribute( 'beans_post_more_link', 'class', 'uk-button uk-button-large' );
beans_output( 'beans_post_more_link_text', '{more please!}' );

beans_remove_action( 'beans_post_meta_categories' );
beans_remove_action( 'beans_post_meta_tags' );
beans_remove_action( 'beans_breadcrumb' );

//Show loaded uikit components
//add_action('wp_enqueue_scripts', 'custom_print_uikit_array');
function custom_print_uikit_array() {
	global $_beans_uikit_enqueued_items;
	ddd( $_beans_uikit_enqueued_items );
}

//Animation
beans_add_attribute( 'beans_sub_menu', 'class', 'uk-animation-fade' );
beans_add_attribute( 'beans_post_more_link', 'class', 'uk-animation-fade' );

//text
beans_add_attribute( 'beans_post_content', 'class', 'uk-text-large' );


// Display posts in a responsive dynamic grid.
add_action( 'wp', 'wst_posts_grid' );
function wst_posts_grid() {

//	 Only apply to posts page.
	if ( ! ( is_home() || is_category() ) ) {
		return;
	}

	//filters
	beans_add_smart_action( 'beans_content_before_markup', 'wst_display_grid_filter' );
	function wst_display_grid_filter() {
		//http://stackoverflow.com/questions/32085360/ordering-get-categories-result-by-predetermined-order
		$categories = array();
		$cat_ids    = array( 66, 19, 40, 3, 772, 17 );
		// A simple foreach loop, to keep things in your required order
		foreach ( $cat_ids as $id ) {
			$categories[] = get_category( $id );
		}
		?>
		<ul id="grid-filter"
		    class="uk-subnav uk-container-center">
			<?php
			foreach ( $categories as $category ) {
				$filter      = $category->name;
				$filter_slug = $category->slug;
				?>
				<li data-uk-filter="<?php echo $filter_slug; ?>"><a href=""><?php
						echo $filter; ?></a></li>
			<?php } ?>
		</ul>
	<?php }

	function wst_get_categories() {
		return get_categories( array(
			'hierarchical' => 1,
			'orderby'      => 'id',
			'order'        => 'ASC',
			'include'      => '66, 19, 40, 3, 772, 17',
		) );

	}

	// Add grid.
	beans_wrap_inner_markup( 'beans_content', 'beans_child_posts_grid', 'div', array(
		'data-uk-grid' => "{gutter: 20, controls: '#grid-filter'}"
	) );
	beans_wrap_markup( 'beans_post', 'beans_child_post_grid_column', 'div', array(
		'class' => 'uk-width-large-1-3 uk-width-medium-1-2'
	) );


	add_filter( 'beans_child_post_grid_column_attributes', 'wst_get_post_attributes' );

	function wst_get_post_attributes( $attributes ) {

		// Get the categories and build and array with its slugs.
		$categories = wp_list_pluck( get_the_category( get_the_ID() ), 'slug' );

		return array_merge( $attributes, array(
			'data-uk-filter' => implode( ',', (array) $categories ) // automatically escaped.
		) );

	}


	// Move the posts pagination after the new grid markup.
	beans_modify_action_hook( 'beans_posts_pagination', 'beans_child_posts_grid_after_markup' );


}

//Single posts printonly custom fields
//add_action( 'beans_post_body', 'wst_display_printonly_custom_fields', 11 );
function wst_display_printonly_custom_fields() {
	if(!is_single()){
		return;
	}
	$title = sanitize_text_field(carbon_get_the_post_meta('post_title'));
	$image = wp_get_attachment_image_src( carbon_get_the_post_meta(  'post_image' ), array( 450, 450 ) );
	$ingredients = carbon_get_the_post_meta('ingredients','complex');
	$preparation = sanitize_text_field(carbon_get_the_post_meta('preparation'));
	d($title);
	d($image);
	d($ingredients);
	d($preparation);
	foreach ( $ingredients as $ingredient ) {
		echo $ingredient['ingredient'];

	}
}

