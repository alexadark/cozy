<?php
// Enqueue uikit assets
beans_add_smart_action( 'beans_uikit_enqueue_scripts', 'wst_enqueue_uikit_assets', 5 );

function wst_enqueue_uikit_assets() {

	// Enqueue uikit overwrite theme folder
	beans_uikit_enqueue_theme( 'beans_child', get_stylesheet_directory_uri() . '/assets/less/uikit' );

	// Add the theme style as a uikit fragment to have access to all the variables
	beans_compiler_add_fragment( 'uikit', get_stylesheet_directory_uri() . '/assets/less/style.less', 'less' );

	beans_compiler_add_fragment( 'uikit', get_stylesheet_directory_uri() . '/assets/js/theme.js', 'js' );

	beans_uikit_enqueue_components( array(  'cover', 'animation' ) );
	beans_uikit_enqueue_components( array(  'grid', 'sticky' ), 'add-ons' );

}
//google fonts
add_action( 'wp_enqueue_scripts', 'wst_add_google_fonts' );
function wst_add_google_fonts() {

	wp_enqueue_style( 'wst-google-fonts', 'https://fonts.googleapis.com/css?family=Lusitana:400,700|Italiana', false );

}





