<?php

function get_seo_title(){
	global $post;
	$seo_title = get_post_meta( $post->ID, '_yoast_wpseo_title', true );
	return $seo_title;
}

function get_seo_description(){
	global $post;
	$desc = get_post_meta( $post->ID, '_yoast_wpseo_metadesc', true );
	return $desc;
}