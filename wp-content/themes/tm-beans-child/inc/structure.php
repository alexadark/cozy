<?php
//add panel box to sidebar widgets
beans_add_attribute('beans_widget_panel_sidebar_primary', 'class', 'uk-panel-box');
beans_add_attribute('beans_widget_panel_sidebar_primary_recent-posts', 'class', 'uk-panel-box-primary uk-contrast');
//Sticky sidebar
//beans_wrap_inner_markup('beans_sidebar_primary','beans_primary_sidebar_content','div', array(
//	'data-uk-sticky'=>'{top:125, boundary: true}',
//));
//utility bar
add_action('beans_header_before_markup','wst_display_utility_bar');
function wst_display_utility_bar(){?>
	<div class="utility-bar">
		<div class="uk-container uk-container-center">
		<?php echo beans_widget_area('utility-bar');?>
		</div>
	</div>
	<?php }
//Sticky Header
//beans_add_attribute('beans_header','data-uk-sticky','top:0');

// Add primary menu search field
beans_add_smart_action( 'beans_primary_menu_append_markup', 'wst_primary_menu_search' );

function wst_primary_menu_search() {

	echo beans_open_markup( 'wst_menu_primary_search', 'div', array(
		'class' => 'tm-search uk-visible-large uk-navbar-content',
		'style' => 'display: none;'
	) );

	get_search_form();

	echo beans_close_markup( 'wst_menu_primary_search', 'div' );

	echo beans_open_markup( 'wst_menu_primary_search_toggle', 'div', array(
		'class' => 'tm-search-toggle uk-visible-large uk-navbar-content uk-display-inline-block uk-contrast'
	) );

	echo beans_open_markup( 'wst_menu_primary_search_icon', 'i', array( 'class' => 'uk-icon-search' ) );
	echo beans_close_markup( 'wst_menu_primary_search_icon', 'i' );

	echo beans_close_markup( 'wst_menu_primary_search_toggle', 'div' );

}

//remove title text
beans_remove_output('beans_site_title_text');

//center menu
//beans_replace_attribute('beans_primary_menu','class','uk-float-right','uk-navbar-center');
//beans_remove_attribute( 'beans_site_branding', 'class', 'uk-float-left' );
//beans_replace_attribute( 'beans_primary_menu', 'class', 'uk-float-right', 'uk-text-center' );
//beans_add_attribute( 'beans_menu[_navbar][_primary]', 'class', 'uk-display-inline-block' );
//beans_add_attribute( 'beans_menu_item', 'class', 'uk-text-left' );

//title tag
beans_remove_attribute('beans_site_title_tag','class', 'uk-text-muted');
beans_remove_attribute('beans_site_title_tag','class','uk-text-small');


//change content and sidebar width
beans_replace_attribute('beans_primary', 'class', 'uk-width-medium-3-4','uk-width-large-7-10');
beans_replace_attribute('beans_sidebar_primary', 'class', 'uk-width-medium-1-4','uk-width-large-3-10');

beans_remove_markup('beans_site');




//meta
//beans_modify_action_priority('beans_post_meta',3);
 //Post image full width
beans_add_attribute( 'beans_post_image', 'class', 'tm-cover-article' );
beans_modify_action_hook('beans_post_image','beans_post_header', 150);

//button comments
beans_replace_attribute('beans_comment_form_submit','class', 'uk-button-primary','uk-button-large');
//fat footer

//add_action( 'beans_footer_before_markup', 'beans_child_footer_widget_area' );

function beans_child_footer_widget_area() {

	?>
	<div class="tm-fat-footer uk-block">
		<div class="uk-container uk-container-center">
			<?php echo beans_widget_area( 'fat-footer' ); ?>
		</div>
	</div>
	<?php

}

beans_add_attribute('beans_widget_area_grid[_fat-footer]','class', 'uk-grid-divider');

//footer
beans_replace_attribute('beans_footer_credit','class', 'uk-text-small uk-text-muted', 'uk-text-medium');
beans_replace_attribute('beans_footer_credit_left','class','uk-align-medium-left', 'uk-text-center uk-display-block');

add_action('beans_footer_credit_right_prepend_markup' ,'wst_display_footer_right_widget');
function wst_display_footer_right_widget(){
	echo beans_widget_area('footer-right');
}



beans_remove_output('beans_footer_credit_right_text');
//off canvas
beans_replace_attribute('beans_primary_menu_offcanvas_button','class',' uk-hidden-large');
beans_remove_output('beans_offcanvas_menu_button');
