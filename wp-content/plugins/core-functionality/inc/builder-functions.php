<?php
/**
 * Display last post
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return string
 */
function wst_display_last_post( $layout ) {
	$query = wst_get_query_posts();
	if ( ! $query->have_posts() ) {
		return '';
	}
	while ( $query->have_posts() ) :
		$query->the_post();
		include( 'views/last-post-view.php' );
	endwhile;
	wp_reset_postdata();
}

/**
 * Display 4 next posts, offset the first one
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return string
 */
function wst_display_four_next_posts( $layout ) {
	$title = sanitize_text_field($layout['four_next_posts_title']);
	$query = wst_get_query_posts( true );
	if ( ! $query->have_posts() ) {
		return '';
	}
	include( 'views/four-posts-view.php' );
	wp_reset_postdata();
}

/**
 * Render each post item in the four next posts
 *
 * @since 1.0.0
 *
 * @param $query
 *
 * @return void
 */
function wst_render_post_item( $query ) {
	while ( $query->have_posts() ) :
		$query->the_post();
		include( 'views/post-item-view.php' );
	endwhile;
}

/**
 * Display 4 posts by id, one on the left, 3 on the right side
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return void
 */
function wst_display_four_block_posts( $layout ) {

	$title = sanitize_text_field($layout['four_block_post_title']);
	include( 'views/four-block-post-view.php' );
}
/**
 * Render the left post in the four block post
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return string
 */
function wst_render_left_post( $layout ) {
	$id    = array( sanitize_text_field($layout['first_post_id'] ));
	$query = wst_query_posts( '', '', $id );

	if ( ! $query->have_posts() ) {
		return '';
	}
	while ( $query->have_posts() ) :
		$query->the_post();
		include( 'views/post-view.php' );


	endwhile;
	wp_reset_postdata();
}

/**
 * Render the three side posts in the four block post
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return string
 */
function wst_render_side_posts( $layout ) {
	$id    = sanitize_text_field($layout['three_side_posts_id']);
	$id    = explode( ",", $id );
	$query = wst_query_posts( '', '', $id );
	if ( ! $query->have_posts() ) {
		return '';
	}
	while ( $query->have_posts() ) :
		$query->the_post();
		include( 'views/side-posts-view.php' );
	endwhile;
	wp_reset_postdata();
}

/**
 * Assign the args to the query for last post and four next posts
 *
 * @since 1.0.0
 *
 * @param bool $is_four_posts
 *
 * @return WP_Query
 */
function wst_get_query_posts( $is_four_posts = false ) {
	$number = $is_four_posts ? 4 : 1;
	$offset = $is_four_posts ? 1 : '';
	$id     = array();

	return wst_query_posts( $number, $offset, $id );
}

/**
 * Query for posts
 *
 * @since 1.0.0
 *
 * @param $number
 * @param $offset
 * @param array $id
 *
 * @return WP_Query
 */
function wst_query_posts( $number, $offset, array $id ) {
	$args  = array(
		'post_type'      => 'post',
		'posts_per_page' => $number,
		'offset'         => $offset,
		'post__in'       => $id
	);
	$query = new WP_Query( $args );

	return $query;
}

/**
 * Display posts choosen by ID in 2 cols
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return void
 */
function wst_display_two_col_posts( $layout ) {

	$title = sanitize_text_field($layout['two_col_posts_title']);
	$id    = sanitize_text_field($layout['post_id']);

	include( 'views/two-col-posts-view.php' );
}

/**
 * Render each post to display in the two col posts layout
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return void
 */
function wst_render_post_to_display( $layout ) {
	if ( $layout['post_item'] ) {
		$ids = $layout['post_item'];
		foreach ( $ids as $id ) {
			$id              = sanitize_text_field($id['post_id']);
			$post_to_display = do_shortcode( "[display-posts id = $id ]" );
			include( 'views/post-to-display-view.php' );

		}
	}
}

/**
 * Display a text editor
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return void
 */
function wst_display_text_area($layout){
	$text_area = $layout['text_editor'];
	echo '<div class="uk-panel-box">';
	echo $text_area;
	echo '</div>';
}
