<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'post_meta', 'layouts' )
         ->set_priority( 'high' )
         ->show_on_post_type( 'page' )
         ->show_on_template( 'builder-page.php' )
         ->add_fields( array(

	         Field::make( 'complex', 'crb_block_layouts' )
	              ->add_fields( 'last_post', array(
		              Field::make( 'text', 'featured_id' )
	              ) )
	              ->add_fields( 'four_next_posts', array(
		              Field::make( 'text', 'four_next_posts_title' )
	              ) )
	              ->add_fields( 'four_block_posts', array(
		              Field::make( 'text', 'four_block_post_title' ),
		              Field::make( 'text', 'first_post_id' ),
		              Field::make( 'text', 'three_side_posts_id' ),
	              ) )
	              ->add_fields( 'two_col_posts', array(
		              Field::make( 'text', 'two_col_posts_title' ),
		              Field::make( 'complex', 'post_item' )
		                   ->add_fields( array(
			                   Field::make( 'text', 'post_id' )
		                   ) )
	              ) )
	              ->add_fields( 'text_area', array(
		              Field::make( 'rich_text', 'text_editor' ),
	              ) )
         ) );

Container::make( 'post_meta', 'print-only' )
         ->set_priority( 'high' )
         ->show_on_post_type( 'post' )
         ->add_fields( array(
	         Field::make( 'text', 'post_title' ),
	         Field::make( 'image', 'post_image' ),
	         Field::make( 'text', 'yield' ),
	         Field::make( 'complex', 'ingredients' )
	              ->add_fields( array(
		              Field::make( 'text', 'ingredient' ),
	              ) ),
	         Field::make( 'rich_text', 'preparation' ),
         ) );
