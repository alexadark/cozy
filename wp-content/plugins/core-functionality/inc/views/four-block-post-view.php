<div class="uk-panel-box four-posts four-block-post">
	<h2 class="uk-text-center home-section-title"><?php echo $title; ?></h2>
	<div class="uk-grid">
		<div class="uk-width-medium-1-2 left-post">
			<?php
			wst_render_left_post( $layout );

			?>

		</div>
		<div class="uk-width-medium-1-2 ">
			<?php
			wst_render_side_posts( $layout );
			?>

		</div>
	</div>
</div>