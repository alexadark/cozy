<?php
$seo_title = get_seo_title();
$desc      = get_seo_description(); ?>
<div class="uk-panel-box last-post">
	<article class="uk-article ">
		<h2 class="uk-article-title">
			<a href="<?php the_permalink(); ?>"
			   title="<?php the_title(); ?>"><?php echo $seo_title; ?></a>
		</h2>
		<div class="tm-article-image tm-cover-article ">
			<a href="<?php the_permalink(); ?>"
			   title="<?php the_title(); ?>">
				<picture><?php the_post_thumbnail( 'full' ) ?></picture>
			</a>
		</div>

		<p><?php echo $desc; ?></p>
	</article>
</div>