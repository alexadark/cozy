<?php $seo_title = get_seo_title();?>
<div class="uk-grid uk-grid-collapse side-posts ">
	<div class="uk-width-4-10 side-posts-image ">
		<a href="<?php the_permalink(); ?>"
		   title="<?php the_title(); ?>">
			<picture><?php the_post_thumbnail( 'thumbnail' ); ?></picture>
		</a>

	</div>
	<div class="uk-width-6-10 side-posts-title ">
		<h2 class="uk-article-title uk-vertical-align-middle">
			<a href="<?php the_permalink(); ?>"
			   title="<?php the_title(); ?>">
				<?php echo $seo_title; ?>
			</a>
		</h2>

	</div>
</div>