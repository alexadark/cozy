<?php
$seo_title = get_seo_title();
$desc = get_seo_description();
if($sidebar){

	echo '<div class="post-container uk-grid uk-grid-collapse" >';

}
?>

<div class="tm-article-image <?php echo $image_class; ?>">
	<a href="<?php the_permalink(); ?>"
	   title=""<?php the_title(); ?>>
		<picture>
			<?php the_post_thumbnail( $image_size ); ?>
		</picture>
	</a>
</div>
<h2 class="uk-article-title <?php echo $title_class; ?>">
	<a href="<?php the_permalink(); ?>"
	   title=""<?php the_title(); ?>>
		<?php echo $seo_title; ?>
	</a>
</h2>
<?php
if($sidebar){
	echo '</div>';
}
else {?>
<p><?php echo $desc; ?></p>
<?php }?>
