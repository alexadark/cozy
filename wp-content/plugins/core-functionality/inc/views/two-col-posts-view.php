<div class="uk-panel-box four-posts ">
	<h2 class="uk-text-center home-section-title"><?php echo $title; ?></h2>
	<div class="uk-grid uk-grid-medium uk-grid-match"
	     data-uk-grid-match="{target:'.uk-article'}"
	     data-uk-grid-margin>
		<?php
		wst_render_post_to_display($layout);
		?>
	</div>
</div>