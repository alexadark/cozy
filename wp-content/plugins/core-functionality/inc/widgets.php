<?php
add_action( 'widgets_init', 'beans_child_widgets_init' );

function beans_child_widgets_init() {

	beans_register_widget_area( array(
		'name' => 'footer right',
		'id' => 'footer-right',
		'description' => 'footer right widget area',
		'beans_type' => 'stack'
	) );

	beans_register_widget_area( array(
		'name' => 'Fat Footer',
		'id' => 'fat-footer',
		'beans_type' => 'grid'
	) );

	beans_register_widget_area( array(
		'name' => 'Utility Bar',
		'id' => 'utility-bar',
		'beans_type' => 'grid'
	) );
	
	

}

