<?php
add_action( 'init', 'register_projects_post_type' );
/**
 * Register cpt for Projects
 */

function register_projects_post_type() {
	$labels = array(
		'name'          => _x( 'Projects', 'post type general name', 'dabootcamp' ),
		'singular_name' => _x( 'Project', 'post type singular name', 'dabootcamp' ),
		'menu_name'     => _x( 'Projects', 'admin menu name', 'dabootcamp' ),
		'add_new'       => _x( 'Add New project', 'faq', 'dabootcamp' ),
		'add_new_item'  => _x( 'Add New project', 'dabootcamp' ),
		'search_items'  => _x( 'Search project', 'dabootcamp' ),
		'not_found'     => _x( 'No project Found', 'dabootcamp' ),

	);
	$args   = array(
		'label'        => __( Projects, 'beans_child' ),
		'labels'       => $labels,
		'public'       => true,
		'menu_icon'    => 'dashicons-sos',
		'supports'     => wst_get_supports( array(
			'comments',
			'trackbacks',
			'post_formats',
			'author',
		) ),
		'taxonomies'   => array(),
//		'rewrite'     => array(
//			'slug' => 'individual-FAQ',
//		),
		'hierarchical' => true,
		'has_archive'  => true,

	);
	register_post_type( 'projects', $args );
}

function wst_get_supports( array $supports_to_exclude ) {
	$all_supports = get_all_post_type_supports( 'post' );


	$all_supports = array_keys( $all_supports );

//	$supports_to_exclude = array(
//		'comments',
//		'trackbacks',
//		'post_formats',
//		'author',
//		'custom-fields',
//		'thumbnail',
//		'excerpt',
//	);


	$supports   = array_filter( $all_supports, function ( $support ) use ( $supports_to_exclude ) {
		return ! in_array( $support, $supports_to_exclude );
	} );
	$supports[] = 'page-attributes';

	return ( $supports );


}
