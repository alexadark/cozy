<?php

add_shortcode( 'display-posts', 'wst_display_posts' );
function wst_display_posts( $attributes ) {
	$default    = array(
		'id'      => '',
		'sidebar' => 0,
	);
	$attributes = shortcode_atts( $default, $attributes, 'display-posts' );

	$html       = '';
	$query      = get_post_records( $attributes );
	$sidebar    = $attributes ['sidebar'];
	$image_size = $sidebar ? 'thumbnail' : 'full';
	$title_class = $sidebar ? 'uk-width-3-5' : '';
	$image_class = $sidebar ? 'uk-width-2-5' : '';





	if ( ! $query->have_posts() ) {
		return '';
	}
	ob_start();

	while ( $query->have_posts() ) {
		$query->the_post();

		include( 'views/post-view.php' );

	}
	$html .= ob_get_clean();

	wp_reset_postdata();

	return $html;
}

function get_post_records( array $attributes ) {
	$args    = array(
		'post_type' => 'any',
		'p'         => $attributes['id'],

	);
	$records = new WP_query( $args );

	return $records;
}
