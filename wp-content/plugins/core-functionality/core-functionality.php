<?php

/*
Plugin Name: Core Functionality
*/



add_action( 'plugins_loaded', 'wpm_load_textdomain' );
/**
 * Load plugin textdomain.
 *
 * @since 1.1.9
 */
function wpm_load_textdomain() {
	load_plugin_textdomain( 'core-functionality', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
}



//require_once( dirname( __FILE__ ) . '/inc/post-types.php' );
//require_once( dirname( __FILE__ ) . '/inc/taxonomies.php' );
require_once( dirname( __FILE__ ) . '/inc/widgets.php' );
require_once( dirname( __FILE__ ) . '/inc/shortcodes.php' );
require_once( dirname( __FILE__ ) . '/inc/builder-functions.php' );
require_once( dirname( __FILE__ ) . '/carbon-fields/carbon-fields-plugin.php' );

add_action('carbon_register_fields', 'crb_register_custom_fields');
function crb_register_custom_fields() {
	include_once(dirname(__FILE__) . '/inc/metaboxes.php');
}
